#!/bin/bash
string=""
IFS='.  ' read -r -a array <<< "$1"

for x in "${array[@]}"
do
        string=$(echo "$string dc=$x,")
done
servername=${array[0]}

string=$(echo ${string//[[:blank:]]/})
dc=${string::-1}
echo $dc

password_hash=$(slappasswd -h {SSHA} -s $3)
sed -i "s/replacedc/$dc/g" /config/db.ldif
echo "olcRootPW: $(slappasswd -h {SSHA} -s $3)" >> /config/db.ldif
sed -i "s/ldapadm/$2/g" /config/db.ldif
ldapmodify -Y EXTERNAL  -H ldapi:/// -f /config/db.ldif

sed -i "s/replacedc/$dc/g" /config/monitor.ldif
sed -i "s/ldapadm/$2/g" /config/monitor.ldif


ldapmodify -Y EXTERNAL  -H ldapi:/// -f /config/monitor.ldif


ldapadd -Y EXTERNAL -H ldapi:/// -f /etc/openldap/schema/cosine.ldif
ldapadd -Y EXTERNAL -H ldapi:/// -f /etc/openldap/schema/nis.ldif
ldapadd -Y EXTERNAL -H ldapi:/// -f /etc/openldap/schema/inetorgperson.ldif

sed -i "s/replacedc/$dc/g" /config/base.ldif
sed -i "s/servername/$servername/g" /config/base.ldif
sed -i "s/ldapadm /$2/g" /config/base.ldif


ldapadd -x -w $3 -D "cn=$2,$dc" -f /config/base.ldif

