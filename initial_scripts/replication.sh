#!/bin/bash
sed -i "s/srverid_rep/$1/g" /config/master01.ldif
sed -i "s/serverip_rep/$2/g" /config/master01.ldif
sed -i "s/serveradm_rep/$3/g" /config/master01.ldif
sed -i "s/replacedc_rep/$4/g" /config/master01.ldif
sed -i "s/serverpass_rep/$5/g" /config/master01.ldif
ldapadd -Y EXTERNAL -H ldapi:/// -f /config/mod_syncprov.ldif
ldapadd -Y EXTERNAL -H ldapi:/// -f /config/syncprov.ldif
ldapmodify -Y EXTERNAL -H ldapi:/// -f /config/master01.ldif
