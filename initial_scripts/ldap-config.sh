#!/bin/bash

if [ "$1" = "new" ]; then
	echo "enter your data dir to mount"
       	read ldap_volume
	while [ 1 ]; do
		if [[ $ldap_volume == /* ]]
		then
			break;
		fi
		echo "please enter full path of your dir"
		read ldap_volume
	done
	if [ ! -z "$(ls -A $ldap_volume)" ]; then
		ls $ldap_volume
		echo " this is not an empty folder"
		exit 1
	fi
	[[ -d $ldap_volume ]] || mkdir $ldap_volume
	echo "enter your host IP:"
        read IP
	echo "enter your host FQDN:" 
        read HOST
	echo "which port do you want to use for ldap web:"
        read WEB_PORT
        echo "the ldap admin user is administrator"
        LDAP_ADMIN_USER="administrator"
	echo "enter ldap administrator password  and then press enter"
        read LDAP_ADMIN_PASSWORD
	
	cp -r ../config/ldap-config/*  $ldap_volume
        chown -R 55:55 $ldap_volume/slapddir
        chown -R 55:55 $ldap_volume/ldapdir

	docker run --restart always -p 389:389 -d  -v $ldap_volume/slapddir:/etc/openldap/slapd.d -v $ldap_volume/ldapdir:/var/lib/ldap \
 --env MAX_NOFILE=8192 --name targoman-ldap-server targoman-ldap-server
	
	docker cp configuration.sh targoman-ldap-server:/
	docker exec -d targoman-ldap-server /configuration.sh $HOST $LDAP_ADMIN_USER $LDAP_ADMIN_PASSWORD 
	docker exec -d targoman-ldap-server rm /configuration.sh
	echo "do you want ldap replication? (y/n) default n:"
	docker cp replication.sh targoman-ldap-server:/
	ldap_repl="n"
        read ldap_repl
	if [ $ldap_repl = "y" ]; then
		echo " how many servers you want to join?"
		read srv_num
		i=0
		 echo "enter server id for replication"
                 read serverid_rep
		while [ $i -lt $srv_num ]; do
				
			let i=i+1 
			echo "collecting server $i information to join"
			echo "enter server 1 ip or domain  you want join to:"
                        read serverip_rep
                        echo "enter admin user of  server $i:"
                        read serveradm_rep
                        echo "enter admin dc of server $i:"
                        read replacedc_rep
                        echo "enter admin password of server $i:"
                        read serverpass_rep
			echo "--------------------------------------------------------------"
			echo $serverpass_rep
			echo "--------------------------------------------------------------"
			docker exec -d targoman-ldap-server /replication.sh $serverid_rep $serverip_rep $serveradm_rep $replacedc_rep $serverpass_rep
			echo " server $i join done succesfully"
		done
                    
     	fi

	


elif [ "$1" = "old" ]; then
        echo "enter your host IP:"
        read IP
        echo "enter your host FQDN:"
        read HOST
        echo "which port do you want to use for ldap web:"
        read WEB_PORT
        echo "enter slapd dir and then press enter"
        read slapddir
        echo $slapddir
        echo "enter ldap lib dir and then press enter"
        read ldapdir
        echo $ldapdir
        docker run --restart always -p 389:389 -d  -v $slapddir:/etc/openldap/slapd.d -v $ldapdir/ldapdir:/var/lib/ldap \
 --env MAX_NOFILE=8192 --name targoman-ldap-server targoman-ldap-server




else
        echo " you have to enter an argument with - new - or - old - value to specify this is a new ldap server or not"
	exit 1
fi


docker run -d -p $WEB_PORT:80 --name targoman-ldap-web --restart on-failure -e HOST=$HOST -e IP=$IP targoman-ldap-web

echo "you can access to ldap web admin panel from - $IP -"
echo "your ldap default admin user is - administrator -"
echo "your ldap web admin default password is - 123 -"
IFS='.  ' read -r -a array <<< "$HOST"

for x in "${array[@]}"
do
        string=$(echo "$string dc=$x,")
done
dc=${string::-1}

echo "your ldap dc configuration is:  $dc"

